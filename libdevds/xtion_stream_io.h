#ifndef XTION_STREAM_IO_H
#define XTION_STREAM_IO_H

//OpenNI2
#include <OpenNI.h>
//OPENCV2
#include "core/core.hpp"
#include "highgui/highgui.hpp"
#include "imgproc/imgproc.hpp"
#include "calib3d/calib3d.hpp"

#include <cstdlib>

namespace dedvs {

	//Re-Definition of the OpenNI Status enum? or own enum for status codes?
	// enum Status {
	// 	STATUS_OK = 0,
	// 	STATUS_ERROR = 1,
	// 	STATUS_NOT_IMPLEMENTED = 2,
	// 	STATUS_NOT_SUPPORTED = 3,
	// 	STATUS_BAD_PARAMETER = 4,
	// 	STATUS_OUT_OF_FLOW = 5,
	// 	STATUS_NO_DEVICE = 6,
	// 	STATUS_TIME_OUT = 102 
	// }

	enum StreamType {
		DEPTH,
		COLOR
	};


	class XtionStreamIO {
	public:
		//XtionStreamIO() no arguments, intialized the drivers etc.
		XtionStreamIO();
		//~Xtionstreamio(stop/destroy streams, close device, openni shutdown)
		~XtionStreamIO();

		//opens any device available device
		int openDevice(); 
		//opens specific uri / or file
		int openDevice(const char *uri);

		void enumerateDevices();

		// creating streams
		int createDepthStream();
		int createRGBStream();
		int createRGBDepthStreams();

		//starting streams
		int startDepthStream();
		int startRGBStream();
		int startRGBDepthStreams();		


		//openni::VideoFrameRef* getDepthFrameref();
		//Returns strideinbyte*height
		size_t getDepthFrameDataPtr(const uint16_t *&pFrame);
		void getDepthFrameCVMat(cv::Mat *pCVMat);

		size_t getColorFrameDataPtr(const openni::RGB888Pixel *&pFrame);
		void getColorFrameCVMat(cv::Mat *pCVMat);

		void printCurrentVideoModes();
		void printDepthVideoModes();
		void printColorVideoModes();

		int setDepthVideoMode(int vm_index);
		int setColorVideoMode(int vm_index);
		int setDepthVideoMode(int res_x, int res_y, int fps, int pixel_format = 100);
		int setColorVideoMode(int res_x, int res_y, int fps, int pixel_format = 200);

		bool isImageRegistrationModeAvailable();
		bool getImageRegistrationMode();
		bool setImageRegistrationMode(bool enable);
		
		bool setDepthColorSyncEnable(bool enable);

		int  recordStreamsToFileSetup(const char * fileName, bool recordDepth, bool recordColor, bool allowLossyCompression = false);
		int  startRecordingToFile();
		void stopRecordingToFile();


		//todo
		// int   getNumberOfFrames(StreamType stream_type);
		// bool  getPlaybackRepeatEnabled ();
		// float getPlaybackSpeed () ;

		// bool seekFreameInPlayback (StreamType stream_type, int frameIndex);
		// bool setPlaybackRepeatEnabled (bool repeat);
		// bool setPlaybackSpeed (float speed);

	protected:

	private:
		openni::Status status_;
		openni::Device device_;
		openni::DeviceInfo device_info_;

		openni::VideoStream depth_;
		openni::VideoMode depth_video_mode_;
		int depth_res_x_;
		int depth_res_y_;
		int depth_fps_;
		openni::VideoFrameRef depth_frame_;

		openni::VideoStream color_;
		openni::VideoMode color_video_mode_;
		int color_res_x_;
		int color_res_y_;
		int color_fps_;
		openni::VideoFrameRef color_frame_;

		openni::Recorder *recorder_;
		openni::PlaybackControl *playbackcontrol_;	
	};
}

//XTION_STREAM_IO_H_
#endif 
