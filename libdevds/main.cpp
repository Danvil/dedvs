#include "xtion_stream_io.h"
#include <iostream>

using namespace cv;

int main()
{
	dedvs::XtionStreamIO xtion_io;
	if(!xtion_io.openDevice())
	{
		std::cout << "# Device Opened" << std::endl;	
	} else {
		return 1;
	}

	if(!xtion_io.createRGBDepthStreams())
	{
		std::cout << "# Streams Created" << std::endl;
	} else {
		return 1;
	}


	if(!xtion_io.startRGBDepthStreams())
	{
		std::cout << "# Streams Started" << std::endl;
	} else {
		return 1;
	}

	//xtion_io.setDepthVideoMode(4);
	xtion_io.setColorVideoMode(4);
	xtion_io.printCurrentVideoModes();

	//RECORDER STUFF
	// if(!xtion_io.recordStreamsToFileSetup("./blub",true,true))
	// 	std::cout << "Recorder created" << std::endl;
	// if(!xtion_io.startRecordingToFile())
	// 	std::cout << "Recording successfully started" << std::endl;
	// cv::waitKey(10000);
	// xtion_io.stopRecordingToFile();

	//cv::namedWindow("Depth", CV_WINDOW_AUTOSIZE );
	cv::namedWindow("Color", CV_WINDOW_AUTOSIZE );
	//cv::Mat depth_mat;
	cv::Mat color_mat;
	cv::Mat grey_mat;

	Size patternsize(4,3); //interior number of corners
	vector<Point2f> corners; //this will be filled by the detected corners

	int framekill = 1;
	

	while(framekill++)
	{
		//std::cout << framekill << std::endl;
		xtion_io.getColorFrameCVMat(&color_mat);
		//xtion_io.getDepthFrameCVMat(&depth_mat);

		cv::cvtColor(color_mat, grey_mat, CV_BGR2GRAY);

		bool patternfound = cv::findChessboardCorners(grey_mat, patternsize, corners,
		        CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
		        + CALIB_CB_FAST_CHECK );

		if(patternfound)
		{
			std::cout << "Found Pattern in Frame " << framekill << std::endl;
		} else {
			std::cout << "Didnt find Pattern in Frame " << framekill << std::endl;
		}

		// if(patternfound)
		//   cv::cornerSubPix(grey_mat, corners, Size(11, 11), Size(-1, -1),
		//     cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));

		//cv::drawChessboardCorners(color_mat, patternsize, Mat(corners), patternfound);

		//imshow("Color", color_mat);
		// imshow("Depth", depth_mat);
		
		if(cv::waitKey(30)>=0)
			break;
	}

	std::cout << "end"<< std::endl;
}