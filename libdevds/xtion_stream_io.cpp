#include "xtion_stream_io.h"
#include <OpenNI.h>


#include <iostream>
//#include <unistd.h>
//#include <stdlib.h>
//#include <termios.h>
//#include <fcntl.h>

namespace dedvs {
	XtionStreamIO::XtionStreamIO() 
	{
		//TODO: THROW EXCEPTION HERE!!!
		status_ = openni::OpenNI::initialize();
		if (status_ != openni::STATUS_OK)
		{
			std::cout << "Initialize failed." <<  std::endl;
			std::cout << openni::OpenNI::getExtendedError() << std::endl;
		}
	}

	XtionStreamIO::~XtionStreamIO() 
	{
		if(recorder_ != NULL)
		{
			recorder_->destroy();
			delete recorder_;
		}
		
		depth_.stop();
		depth_.destroy();
		color_.stop();
		color_.destroy();
		device_.close();
	    openni::OpenNI::shutdown();
	}

	int XtionStreamIO::openDevice() 
	{
		status_ = device_.open(openni::ANY_DEVICE);
		if (status_ != openni::STATUS_OK)
		{
			std::cout << "Couldn't open device" <<  std::endl;
			std::cout << openni::OpenNI::getExtendedError() << std::endl;
			return 2;
		}
		return 0;
	}

	int XtionStreamIO::openDevice(const char *uri) 
	{
		status_ = device_.open(uri);
		if (status_ != openni::STATUS_OK)
		{
			std::cout << "Couldn't open device" <<  std::endl;
			std::cout << openni::OpenNI::getExtendedError() << std::endl;
			return 2;
		}

		if(device_.isFile())
			playbackcontrol_ = device_.getPlaybackControl();

		return 0;
	}

	void XtionStreamIO::enumerateDevices()
	{
		openni::Array<openni::DeviceInfo> deviceinfos;
		openni::OpenNI::enumerateDevices(&deviceinfos);

		for(int i=0; i<deviceinfos.getSize(); i++)
		{
			std::cout << std::endl;
			std::cout << "###DEVICE INFO###" << std::endl;
			std::cout << "Name: " << deviceinfos[i].getName() << " | URI: " << deviceinfos[i].getUri() << std::endl;
			std::cout << "Vendor: " << deviceinfos[i].getVendor() << " USB Product Id" << deviceinfos[i].getUsbProductId() << std::endl; 
			std::cout << std::endl;
		}
	}

	int XtionStreamIO::createDepthStream()
	{
		if (device_.getSensorInfo(openni::SENSOR_DEPTH) != NULL)
		{
			status_ = depth_.create(device_, openni::SENSOR_DEPTH);
			if (status_ != openni::STATUS_OK)
			{
				std::cout << "Couldn't create depth stream" <<  std::endl;
				std::cout << openni::OpenNI::getExtendedError() << std::endl;
				return 3;
			}
			if (depth_.isValid())
		    {
				depth_video_mode_ = depth_.getVideoMode();	
			    
			    depth_res_x_ 	= depth_video_mode_.getResolutionX();
			    depth_res_y_	= depth_video_mode_.getResolutionY();
			    depth_fps_ 		= depth_video_mode_.getFps();
			}
		} else  {
			return 3;
		}

		return 0;
	}

	int XtionStreamIO::createRGBStream()
	{
		if (device_.getSensorInfo(openni::SENSOR_COLOR) != NULL)
		{
			status_ = color_.create(device_, openni::SENSOR_COLOR);
			if (status_ != openni::STATUS_OK)
			{
				std::cout << "Couldn't create color stream" <<  std::endl;
				std::cout << openni::OpenNI::getExtendedError() << std::endl;
				return 3;
			}
			if (color_.isValid())
		    {
				color_video_mode_ = color_.getVideoMode();	
			    
			    color_res_x_ 	= color_video_mode_.getResolutionX();
			    color_res_y_	= color_video_mode_.getResolutionY();
			    color_fps_ 		= color_video_mode_.getFps();
			}
		} else  {
			return 3;
		}

		return 0;
	}

	int XtionStreamIO::createRGBDepthStreams()
	{
		if(createDepthStream() != 0)
			return 3;
		if(createRGBStream() != 0)
			return 3;

		return 0;
	}

	int XtionStreamIO::startDepthStream()
	{
		status_ = depth_.start();
		if (status_ != openni::STATUS_OK)
		{
			std::cout << "Couldn't start the depth stream" <<  std::endl;
			std::cout << openni::OpenNI::getExtendedError() << std::endl;
			return 4;
		}

		return 0;
	}

	int XtionStreamIO::startRGBStream()
	{
		status_ = color_.start();
		if (status_ != openni::STATUS_OK)
		{
			std::cout << "Couldn't start the color stream" <<  std::endl;
			std::cout << openni::OpenNI::getExtendedError() << std::endl;
			return 4;
		}

		return 0;
	}

	int XtionStreamIO::startRGBDepthStreams()
	{
		if(startDepthStream() != 0)
			return 4;
		if(startRGBStream() != 0)
			return 4;

		return 0;
	}

	size_t XtionStreamIO::getDepthFrameDataPtr(const uint16_t* &pFrame)
	{
		status_ = depth_.readFrame(&depth_frame_);
		if (status_ != openni::STATUS_OK)
		{
			std::cout << "Reading depth frame failed!" << std::endl;
			pFrame = NULL;
			return 0;
		}
		pFrame = (const uint16_t*) depth_frame_.getData();
		return (size_t) (depth_frame_.getStrideInBytes() * depth_frame_.getHeight());
	}

	void XtionStreamIO::getDepthFrameCVMat(cv::Mat *pCVMat)
	{
		uint16_t const *frame;

		if(depth_.getVideoMode().getPixelFormat() != openni::PIXEL_FORMAT_DEPTH_1_MM)
			return;

		if(pCVMat->cols != depth_res_x_ || pCVMat->rows != depth_res_y_) 
		{
	        pCVMat->create(cv::Size(depth_res_x_, depth_res_y_), CV_16SC1);//CV_16U
    	}

		size_t stride_in_bytes = getDepthFrameDataPtr(frame);
		memcpy(pCVMat->data, frame, stride_in_bytes);
	}	

	size_t XtionStreamIO::getColorFrameDataPtr(const openni::RGB888Pixel *&pFrame)
	{
		status_ = color_.readFrame(&color_frame_);
		if (status_ != openni::STATUS_OK)
		{
			std::cout << "Reading color frame failed!" << std::endl;
			pFrame = NULL;
			return 0;
		}
		pFrame = (const openni::RGB888Pixel*) color_frame_.getData();
		return (size_t) (color_frame_.getStrideInBytes() * color_frame_.getHeight());
	}

	void XtionStreamIO::getColorFrameCVMat(cv::Mat *pCVMat)
	{
		const openni::RGB888Pixel *frame;

		if(color_.getVideoMode().getPixelFormat() != openni::PIXEL_FORMAT_RGB888)
			return;

		if(pCVMat->cols != color_res_x_ || pCVMat->rows != color_res_y_) 
		{
	        pCVMat->create(cv::Size(color_res_x_, color_res_y_), CV_8UC3);
    	}

		size_t stride_in_bytes = getColorFrameDataPtr(frame);
		memcpy(pCVMat->data, frame, stride_in_bytes);
		cvtColor(*pCVMat, *pCVMat, CV_RGB2BGR);
	}	

	void XtionStreamIO::printCurrentVideoModes()
	{
		openni::VideoMode vm_depth = depth_.getVideoMode();
		openni::VideoMode vm_color = color_.getVideoMode();
		std::cout << "Video Mode for Depth Stream |" << "  X: " << vm_depth.getResolutionX() << "  Y: " 
						<< vm_depth.getResolutionY() << "  FPS: " << vm_depth.getFps() 
						<< " (" << vm_depth.getPixelFormat() << ")"<< std::endl;
		std::cout << "Video Mode for Color Stream |" << "  X: " << vm_color.getResolutionX() << "  Y: " 
						<< vm_color.getResolutionY() << "  FPS: " << vm_color.getFps() 
						<< " (" << vm_color.getPixelFormat() << ")"<< std::endl;						
	}

	void XtionStreamIO::printDepthVideoModes()
	{
		const openni::Array<openni::VideoMode>& vm = depth_.getSensorInfo().getSupportedVideoModes();
		for(int i=0; i < vm.getSize(); i++)
		{
			std::cout << i << " #:" << "  X: " << vm[i].getResolutionX() << "  Y: " 
						<< vm[i].getResolutionY() << "  FPS: " << vm[i].getFps() 
						<< " (" << vm[i].getPixelFormat() << ")"<< std::endl;
		}
		std::cout << "(PIXEL_FORMAT_DEPTH_1_MM = 101 | PIXEL_FORMAT_DEPTH_100_UM = 101)" << std::endl;
	}

	void XtionStreamIO::printColorVideoModes()
	{
		const openni::Array<openni::VideoMode>& vm = color_.getSensorInfo().getSupportedVideoModes();
		for(int i=0; i < vm.getSize(); i++)
		{
			std::cout << i << " #:" << "  X: " << vm[i].getResolutionX() << "  Y: " 
						<< vm[i].getResolutionY() << "  FPS: " << vm[i].getFps() 
						<< " (" << vm[i].getPixelFormat() << ")"<< std::endl;
		}
		std::cout << "(PIXEL_FORMAT_RGB888 = 200 | PIXEL_FORMAT_YUV422 = 201 | PIXEL_FORMAT_GRAY8 = 202)" << std::endl; 
	}

	int XtionStreamIO::setDepthVideoMode(int vm_index)
	{
		const openni::Array<openni::VideoMode>& vm = depth_.getSensorInfo().getSupportedVideoModes();
		depth_.setVideoMode(vm[vm_index]);
		depth_res_x_ 	= vm[vm_index].getResolutionX();
	    depth_res_y_	= vm[vm_index].getResolutionY();
	    depth_fps_ 		= vm[vm_index].getFps();
	}

	int XtionStreamIO::setColorVideoMode(int vm_index)
	{
		const openni::Array<openni::VideoMode>& vm = color_.getSensorInfo().getSupportedVideoModes();
		color_.setVideoMode(vm[vm_index]);
		color_res_x_ 	= vm[vm_index].getResolutionX();
	    color_res_y_	= vm[vm_index].getResolutionY();
	    color_fps_ 		= vm[vm_index].getFps();
	}

	int XtionStreamIO::setDepthVideoMode(int res_x, int res_y, int fps, int pixel_format)
	{
		openni::VideoMode vm;

		vm.setResolution(res_x,res_y);
		vm.setFps(fps);
		switch(pixel_format)
		{
			case 100: vm.setPixelFormat(openni::PIXEL_FORMAT_DEPTH_1_MM); break;
			case 101: vm.setPixelFormat(openni::PIXEL_FORMAT_DEPTH_100_UM); break;
			default	: vm.setPixelFormat(openni::PIXEL_FORMAT_DEPTH_1_MM);
		}

		depth_.setVideoMode(vm);

		depth_res_x_ 	= vm.getResolutionX();
	    depth_res_y_	= vm.getResolutionY();
	    depth_fps_ 		= vm.getFps();
	}

	int XtionStreamIO::setColorVideoMode(int res_x, int res_y, int fps, int pixel_format)
	{
		openni::VideoMode vm;

		vm.setResolution(res_x,res_y);
		vm.setFps(fps);
		switch(pixel_format)
		{
			case 200: vm.setPixelFormat(openni::PIXEL_FORMAT_RGB888); break;
			case 201: vm.setPixelFormat(openni::PIXEL_FORMAT_YUV422); break;
			case 202: vm.setPixelFormat(openni::PIXEL_FORMAT_GRAY8); break;
			default	: vm.setPixelFormat(openni::PIXEL_FORMAT_RGB888);
		}

		color_.setVideoMode(vm);

		color_res_x_ 	= vm.getResolutionX();
	    color_res_y_	= vm.getResolutionY();
	    color_fps_ 		= vm.getFps();
	}

	bool XtionStreamIO::isImageRegistrationModeAvailable()
	{
		return device_.isImageRegistrationModeSupported(device_.getImageRegistrationMode());
	}

	bool XtionStreamIO::getImageRegistrationMode()
	{
		return device_.getImageRegistrationMode() ? true : false;
	}

	bool XtionStreamIO::setImageRegistrationMode(bool enable)
	{
		if(enable == true)
			status_ = device_.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
		else
			status_ = device_.setImageRegistrationMode(openni::IMAGE_REGISTRATION_OFF);

		return (status_ != openni::STATUS_OK) ? false : true;
	}

	bool XtionStreamIO::setDepthColorSyncEnable(bool enable)
	{
		status_ = device_.setDepthColorSyncEnabled(enable);
		return (status_ != openni::STATUS_OK) ? false : true;
	}

	int XtionStreamIO::recordStreamsToFileSetup(const char * fileName, bool recordDepth,
										   		bool recordColor, bool allowLossyCompression)
	{
		recorder_ 	= new openni::Recorder;
		status_ 	= recorder_->create(fileName);

		if(status_ != openni::STATUS_OK)
			return 1;

		if(recordDepth && recorder_->isValid())
			recorder_->attach(depth_,allowLossyCompression);
		if(recordColor && recorder_->isValid())
			recorder_->attach(color_,allowLossyCompression);

		return 0;
	}

	int XtionStreamIO::startRecordingToFile()
	{
		status_ = recorder_->start();
		if(status_ != openni::STATUS_OK)
			return 1;
		else
			return 0;
	}

	void XtionStreamIO::stopRecordingToFile()
	{
		recorder_->stop();
	}

	// int   XtionStreamIO::getNumberOfFrames(StreamType stream_type)
	// {
	// 	if(!playbackcontrol_->isValid())
	// 		return 1;

	// 	if(stream_type == DEPTH)
	// 		return playbackcontrol_->getNumberOfFrames(depth_);
	// 	else if(stream_type == COLOR)
	// 		return playbackcontrol_->getNumberOfFrames(color_);
	// 	else
	// 		return 0;
	// }

	// bool  XtionStreamIO::getPlaybackRepeatEnabled()
	// {
	// 	if(!playbackcontrol_->isValid())
	// 		return 1;

	// }

	// float XtionStreamIO::getPlaybackSpeed()
	// {
	// 	if(!playbackcontrol_->isValid())
	// 		return 1;


	// }

	// bool XtionStreamIO::seekFreameInPlayback (StreamType stream_type, int frameIndex);
	// bool XtionStreamIO::setPlaybackRepeatEnabled (bool repeat);
	// bool XtionStreamIO::setPlaybackSpeed (float speed);

}
