/*****************************************************************************
*                                                                            *
*  OpenNI 2.x Alpha                                                          *
*  Copyright (C) 2012 PrimeSense Ltd.                                        *
*                                                                            *
*  This file is part of OpenNI.                                              *
*                                                                            *
*  Licensed under the Apache License, Version 2.0 (the "License");           *
*  you may not use this file except in compliance with the License.          *
*  You may obtain a copy of the License at                                   *
*                                                                            *
*      http://www.apache.org/licenses/LICENSE-2.0                            *
*                                                                            *
*  Unless required by applicable law or agreed to in writing, software       *
*  distributed under the License is distributed on an "AS IS" BASIS,         *
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
*  See the License for the specific language governing permissions and       *
*  limitations under the License.                                            *
*                                                                            *
*****************************************************************************/
#include <OpenNI.h>
//#include <Common/OniSampleUtilities.h>
#include <iostream>
#include "core/core.hpp"
#include "highgui/highgui.hpp"
#include "imgproc/imgproc.hpp"
#include "calib3d/calib3d.hpp"


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>

using namespace cv;
using namespace openni;

int wasKeyboardHit()
{
	struct termios oldt, newt;
	int ch;
	int oldf;

	// don't echo and don't wait for ENTER
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
	
	// make it non-blocking (so we can check without waiting)
	if (0 != fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK))
	{
		return 0;
	}

	ch = getchar();

	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	if (0 != fcntl(STDIN_FILENO, F_SETFL, oldf))
	{
		return 0;
	}

	if(ch != EOF)
	{
		ungetc(ch, stdin);
		return 1;
	}

	return 0;
}

int main(int argc, char** argv)
{
	Status rc = OpenNI::initialize();
	if (rc != STATUS_OK)
	{
		printf("Initialize failed\n%s\n", OpenNI::getExtendedError());
		return 1;
	}

	Device device;
	rc = device.open(ANY_DEVICE);
	if (rc != STATUS_OK)
	{
		printf("Couldn't open device\n%s\n", OpenNI::getExtendedError());
		return 2;
	}

	// DeviceInfo device_info = device.getDeviceInfo();
	// std::cout << "###DEVICE INFO###" << std::endl;
	// std::cout << "Name: " << device_info.getName() << " | URI: " << device_info.getUri() << std::endl;
	// std::cout << "Vendor: " << device_info.getVendor() << "  " << device_info.getUsbProductId() << std::endl; 
	// std::cout << "##############################################" << std::endl;

	Array<DeviceInfo> deviceinfos;
	OpenNI::enumerateDevices(&deviceinfos);

	for(int i=0; i<deviceinfos.getSize(); i++)
	{
		std::cout << std::endl;
		std::cout << "###DEVICE INFO###" << std::endl;
		std::cout << "Name: " << deviceinfos[i].getName() << " | URI: " << deviceinfos[i].getUri() << std::endl;
		std::cout << "Vendor: " << deviceinfos[i].getVendor() << "  " << deviceinfos[i].getUsbProductId() << std::endl; 
		std::cout << "##############################################" << std::endl;
	}

	VideoStream depth,color;

	if (device.getSensorInfo(SENSOR_DEPTH) != NULL)
	{
		rc = depth.create(device, SENSOR_DEPTH);
		if (rc != STATUS_OK)
		{
			printf("Couldn't create depth stream\n%s\n", OpenNI::getExtendedError());
			return 3;
		}
	}

	if (device.getSensorInfo(SENSOR_COLOR ) != NULL)
	{
		rc = color.create(device, SENSOR_COLOR );
		if (rc != STATUS_OK)
		{
			printf("Couldn't create color stream\n%s\n", OpenNI::getExtendedError());
			return 3;
		}
	}

	std::cout << "Current mode: " << device.getImageRegistrationMode() << std::endl;
	std::cout << "Device valid: " << device.isValid() << std::endl;

	if(device.isImageRegistrationModeSupported(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR ))
	{
		rc = device.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR );
		std::cout << "RC: " << rc << std::endl;
		if(rc != STATUS_OK)
		{
			std::cout << "Image registration (depth to color) failed" <<  std::endl << OpenNI::getExtendedError() << std::endl;
		}
	}
	std::cout << "mode now: " <<  device.getImageRegistrationMode() << std::endl;

	rc = depth.start();
	if (rc != STATUS_OK)
	{
		printf("Couldn't start the depth stream\n%s\n", OpenNI::getExtendedError());
		return 4;
	}

	rc = color.start();
	if (rc != STATUS_OK)
	{
		printf("Couldn't start the color stream\n%s\n", OpenNI::getExtendedError());
		return 4;
	}

    VideoMode depthVideoMode;
    VideoMode colorVideoMode;

    int depthWidth, depthHeight, colorWidth, colorHeight;

	if (depth.isValid() && color.isValid())
    {
	    depthVideoMode = depth.getVideoMode();
	    colorVideoMode = color.getVideoMode();
	    
	    depthWidth = depthVideoMode.getResolutionX();
	    depthHeight = depthVideoMode.getResolutionY();
	    colorWidth = colorVideoMode.getResolutionX();
	    colorHeight = colorVideoMode.getResolutionY();
	}

	VideoFrameRef depthFrame, colorFrame;
	Mat depthmap,rgbmap;
	if(depthmap.cols != depthWidth || depthmap.rows != depthHeight || rgbmap.cols != colorWidth || rgbmap.rows != colorHeight) {
        depthmap.create(depthHeight,depthWidth,CV_16SC1);
        rgbmap.create(colorHeight,colorWidth,CV_8UC3);
    }

	namedWindow("Depth", CV_WINDOW_AUTOSIZE );
    namedWindow("Color", CV_WINDOW_AUTOSIZE );


	Size patternsize(9,6); //interior number of corners
	vector<Point2f> corners; //this will be filled by the detected corners

//CALIB_CB_FAST_CHECK saves a lot of time on images
//that do not contain any chessboard corners





	int i=0;
	while (true)
	{
		rc = depth.readFrame(&depthFrame);
		if (rc != STATUS_OK)
		{
			printf("Wait failed\n");
			continue;
		}
		std::cout << "Got my " << i << "th depth frame" << std::endl;

		rc = color.readFrame(&colorFrame);
		if (rc != STATUS_OK)
		{
			printf("Wait failed\n");
			continue;
		}

		std::cout << "Got my " << i++ << "th color frame" << std::endl;

        const openni::RGB888Pixel* pImageRow = (const openni::RGB888Pixel*)colorFrame.getData();
        memcpy(rgbmap.data,pImageRow,colorFrame.getStrideInBytes() * colorFrame.getHeight());
        cvtColor(rgbmap, rgbmap, CV_RGB2BGR);

        const openni::DepthPixel* pDepthRow = (const openni::DepthPixel*)depthFrame.getData();
        memcpy(depthmap.data,pDepthRow,depthFrame.getStrideInBytes() * depthFrame.getHeight());

		imshow("Depth", depthmap);

		bool patternfound = findChessboardCorners(rgbmap, patternsize, corners,
		        CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
		        + CALIB_CB_FAST_CHECK);

		Mat greyMat;
		cvtColor(rgbmap, greyMat, CV_BGR2GRAY);

		if(patternfound)
		  cornerSubPix(greyMat, corners, Size(11, 11), Size(-1, -1),
		    TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));

		drawChessboardCorners(rgbmap, patternsize, Mat(corners), patternfound);

		imshow("Color", rgbmap);



		if(waitKey(10)>=0)
			break;
	}


	//stop, then destroy stream
	depth.stop();
	depth.destroy();
	color.stop();
	color.destroy();
	device.close();
    OpenNI::shutdown();

	return 0;
}
